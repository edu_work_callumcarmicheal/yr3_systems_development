//
// Created by callu on 27/11/2018.
//

#ifndef TESTING_TEST_H
#define TESTING_TEST_H

/** ========== Macros ========== */

#define clrscr() \
    printf("\e[1;1H\e[2J")
#define MENU_TEXT_SIZE 255

#define bool	_Bool
#define true	1
#define false	0

/** ========== Menu Definitions ========== */

// Item and Menu Types
enum   ItemType;
struct MenuItem;
struct MenuItems;
struct MenuSettings;
struct Menu;

// Environment functions
void environment_setup(void);
void environment_end(void);

// Menu functions
struct  Menu*       menu_create(char Title[MENU_TEXT_SIZE]);
        void        menu_free(struct Menu* pMenu, bool freeItems);
struct  MenuItem*   menu_item_add(struct Menu* pMenu, enum ItemType Type, char Text[MENU_TEXT_SIZE], int Tag);
        int         menu_item_find_index(struct Menu* pMenu, struct MenuItem* pWanted);
        int         menu_item_find_next_type_index_up(struct Menu *pMenu, struct MenuItem *pItem, enum ItemType type, int currentIndex);
        int         menu_item_find_next_type_index_down(struct Menu *pMenu, struct MenuItem *pItem, enum ItemType type, int currentIndex);
        int         menu_item_find_nearest_type_index(struct Menu* pMenu, struct MenuItem* pItem, enum ItemType type, bool deletion);
        void        menu_item_free(struct Menu* pMenu, struct MenuItem* pItem);
        void        menu_item_free_all(struct Menu *pMenu);

// To render the menu you need to have NCURSES included before this.
#ifdef __NCURSES_H
        int         menu_render(WINDOW *pWin, struct Menu *pMenu);
#endif

#endif //TESTING_TEST_H
