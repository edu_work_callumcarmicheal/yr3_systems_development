// This is just a bunch of cut code but might end up being useful later!
//

void menu_item_free_old(struct Menu* pMenu, struct MenuItem* pItem) {
    // Get the next and previous items
    struct MenuItem* Previous = pItem->pPrevious;
    struct MenuItem* Next = pItem->pNext;

    // Check and update if we are the highlighted index
    struct MenuItem* pCurrent = pMenu->Items.pFirst; // Get the first item

    // Go through the linked list
    int i = 0;
    while (pCurrent != NULL) {
        if (i == pMenu->_HighlightIndex) {
            // Check if the current item is pItem
            if (pCurrent == pItem) {
                // If we have a previous item
                if (pItem->pPrevious != NULL) {
                    // TODO: Find the nearest BUTTON not just TEXT
                    pMenu->_HighlightIndex--;
                }
                // If we have a next item
                else if (pItem->pNext != NULL) {
                    // TODO: Find the nearest BUTTON (below), this also means when we pop off \
                    //   items the _HighlightIndex should not change unless the next item is a text label.
                    pMenu->_HighlightIndex++;
                }
                // Fallback to index 0
                else {
                    pMenu->_HighlightIndex = 0;
                }
            }

            // Break the loop because we have the index we want
            break;
        }

        pCurrent = pCurrent->pNext;
        i++;
    }

    // Make the previous item point to the new next item
    if (Next != NULL) {
        if (Previous != NULL) {
            // Update the previous item to the new link
            Previous->pNext = Next;
        }

        // Set the first element and remove the previous
        else {
            pMenu->Items.pFirst = Next;
            Next->pPrevious = NULL;
        }
    }

    if (Previous == NULL) {
        Next->pPrevious = NULL;
    }

    // Update the last item in the list if we are it
    if (pMenu->Items.pLast == pItem && Previous != NULL) {
        pMenu->Items.pLast = Previous;
        Previous->pNext = NULL;
    }

    // Update our counter
    pMenu->Items.Count--;

    // Check if the active item is the current item
    if (pMenu->pSelectedItem == pItem)
        pMenu->pSelectedItem = NULL;

    // Free our item
    free (pItem);
}

void old_menu_test(void) {
    // Create our example list
    struct MenuItem mSection = { IT_TEXT,   "Buttons",    0, NULL };
    struct MenuItem mButton1 = { IT_BUTTON, "Button 1",   0, NULL };
    struct MenuItem mButton2 = { IT_BUTTON, "Button 2",   0, NULL };

    // Link our list
    mSection.pNext = &mButton1;
    mButton1.pNext = &mButton2;

    printw("Before Window!");
    refresh();

    struct Menu menu = {"Test Menu", 1, NULL, &mSection};
}


