// TODO: Remove all unneeded imports
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <pthread.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <unistd.h>
#include "rdwrn.h"
#include <curses.h>
#include <ncurses.h>
#include "test.h"

// Macros
#define clrscr() \
    printf("\e[1;1H\e[2J")

// Definitions
#define MENU_TEXT_SIZE 255

/**
 * Menu item type
 */
enum ItemType   { IT_TEXT, IT_BUTTON };

/**
 * The menu item
 */
struct MenuItem {
    enum    ItemType        Type;
            char            Text[MENU_TEXT_SIZE];
            int             Tag;
    struct  MenuItem*       pPrevious;
    struct  MenuItem*       pNext;
};

/**
 * Handles the menu items.
 */
struct MenuItems {
    struct  MenuItem*       pFirst; // First item in the list
    struct  MenuItem*       pLast;  // Last item in the list
            int             Count;  // The amount of items in the list (starts at 1)
};

/**
 * Callback for the menu rendering (prerender)
 */
typedef void (*CBMenuRender)(WINDOW*, struct Menu*);

/**
 * The Menu Settings
 */
struct MenuSettings {
            int             RenderSize;     // Amount of items to render.
            int             ViewStartIndex; // States the start of the rendering view.
            CBMenuRender    pRenderCb;      // Callback to prerender for each menu.
};

/**
 * The menu structure
 */
struct Menu {
            char            Title[MENU_TEXT_SIZE];  // Menu Title
            int             HighlightedIndex;       // Currently Active Item
    struct  MenuItem*       pSelectedItem;          // Pointer to Selection
    struct  MenuItems       Items;                  // Items in the Menu
    struct  MenuSettings    Settings;               // Menu Settings
};

/**
 * Setup the environment
 */
void environment_setup(void) {
    // Init curses
    initscr();

    // Settings
    start_color();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
}

/**
 * End the environment
 */
void environment_end(void) {
    endwin();
    setvbuf(stdout, NULL, _IOLBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
}

/**
 * Create a menu
 *
 * @param Title
 * @return
 */
struct Menu* menu_create(char Title[MENU_TEXT_SIZE]) {
    // Create menu
    struct Menu* menu = malloc(sizeof(struct Menu));

    strncpy(menu->Title, Title, sizeof(menu->Title));
    menu->HighlightedIndex = -1;
    menu->pSelectedItem = NULL;

    // Setup default settings values
    menu->Settings.RenderSize = 20; // Display up to 20 items by default
    menu->Settings.ViewStartIndex = 0; // Start at 0 always.
    menu->Settings.pRenderCb = NULL;

    return menu;
}

/**
 * Free a menu
 *
 * @param pMenu
 * @param freeItems
 */
void menu_free(struct Menu* pMenu, bool freeItems) {
    if (freeItems)
        // Free the menu items
        menu_item_free_all(pMenu);

    // Free the menu
    free(pMenu);
}

/**
 * Add an item to the menu
 *
 * @param pMenu The menu item.
 * @param Type The item type.
 * @param Text The text of the item.
 * @param Tag A tag to identify the item.
 * @return
 */
struct MenuItem* menu_item_add(struct Menu* pMenu, enum ItemType Type, char Text[MENU_TEXT_SIZE], int Tag) {
    // Create item, Set the Type, Copy the Text and Set the Tag.
    struct MenuItem* item = malloc(sizeof(struct MenuItem));
    item->Type = Type;
    strncpy(item->Text, Text, sizeof(item->Text));
    item->Tag  = Tag;

    // Set the first item
    if (pMenu->Items.Count == 0) {
        pMenu->Items.pFirst = item;

        // highlight index to 0 if the type is a button
        if (item->Type == IT_BUTTON)
            pMenu->HighlightedIndex = 0;
    }

    // We have items
    else {
        // Add to the end of the list
        pMenu->Items.pLast->pNext = item;

        // Set the previous item
        item->pPrevious = pMenu->Items.pLast;
    }

    // Increment our count and set the last item
    pMenu->Items.Count++;
    pMenu->Items.pLast = item;
    return item;
}

/**
 * Find index for menu item
 *
 * @param pMenu
 * @param pWanted
 * @return Index number
 */
int menu_item_find_index(struct Menu* pMenu, struct MenuItem* pWanted) {
    // Get the first item
    struct MenuItem* pItem = pMenu->Items.pFirst;

    // Go through the linked list
    int x = 0;
    while (pItem != NULL) {
        // If we are the wanted item return the index
        if (pItem == pWanted)
            return x;

        // Loop the next item
        pItem = pItem->pNext;
        x++;
    }

    // We could not find it return -1
    return -1;
}


/**
 * Find the next item by a item type in the downwards direction
 *
 * @return NULL if not found.
 */
struct MenuItem* menu_item_find_tag(struct Menu *pMenu, int Tag) {
    // If we dont have any items return null
    if (pMenu->Items.Count == 0)
        return NULL;

    // Loop the items
    struct MenuItem* pCurrent = pMenu->Items.pFirst;
    while (pCurrent != NULL) {
        // If we have found the tag return the item
        if (pCurrent->Tag == Tag)
            return pCurrent;

        // Loop to the next item
        pCurrent = pCurrent->pNext;
    }

    // We did not find it return NULL
    return NULL;
}


/**
 * Find the next item by a item type in the upwards direction
 *  
 * @return [-1 For not found, >0 When found]
 */
int menu_item_find_next_type_index_up(struct Menu *pMenu, struct MenuItem *pItem, enum ItemType type,
                                      int currentIndex) {
    // Set the current index to +1 because we are getting the next item
	int index = currentIndex + 1;

	// Loop the items
	struct MenuItem* pCurrent = pItem->pNext;
	while (pCurrent != NULL) {
		// If we found it return the index
	    if (pCurrent->Type == type)
			return index;

		// Next item
		pCurrent = pCurrent->pNext;
		index++;
	}	

	// Return -1, we could not find it.
	return -1;
}

/**
 * Find the next item by a item type in the downwards direction
 *
 * @return [-1 For not found, >0 When found]
 */
int menu_item_find_next_type_index_down(struct Menu *pMenu, struct MenuItem *pItem, enum ItemType type,
                                        int currentIndex) {
    int index = currentIndex - 1;
    struct MenuItem* pCurrent = pItem->pPrevious;

    while (pCurrent != NULL) {
        if (pCurrent->Type == type)
            return index;

        pCurrent = pCurrent->pPrevious;
        index--;
    }

    return -1;
}

/**
 * Search for the nearest index of
 *
 * @param pMenu The menu that contains the items
 * @param pItem The item we start the search froom
 * @param type The menu type we are looking for
 * @param deletion States if we are about to pop the current item of the stack
 * @return
 */
int menu_item_find_nearest_type_index(struct Menu* pMenu, struct MenuItem* pItem, enum ItemType type,
                                      bool deletion) {
	// Check if the list is empty then just return -1
	if (pMenu->Items.Count == 0
        // This just states if we are popping a item off and still return -1
		|| (deletion && pMenu->Items.Count == 1))
		return -1;

	// Check if we are the only item in the list
    if (pMenu->Items.Count == 1)
        return -1;

    // Setup the indexes.
	int currentIndex = menu_item_find_index(pMenu, pItem);
	int newIndex = -1;
	
	// If we are the first item then we must go up
	if (currentIndex == 0) {
	    newIndex = menu_item_find_next_type_index_up(pMenu, pItem, type, currentIndex);
	}

	// If we are the last item then we must go down
	else if (currentIndex + 1 == pMenu->Items.Count) {
		newIndex = menu_item_find_next_type_index_down(pMenu, pItem, type, currentIndex);
	}

	// We are in the center of the list, we gotta check both directions.
	// for this we will first go down the list and then up.
	else {
		// If we failed to find a item going down then lets search up.
        newIndex = menu_item_find_next_type_index_down(pMenu, pItem, type, currentIndex);

        if (newIndex == -1) {
            // We failed to find a type going up, so we now want to search down
            newIndex = menu_item_find_next_type_index_up(pMenu, pItem, type, currentIndex);

            // If we are deleting the item then we need to account for it shifting the menu.
            if (newIndex > -1 && deletion)
                newIndex--;
		}
    }
	
	// Return our result
    return newIndex;
}	

/**
 * Free a specific menu item.
 *
 * @param pMenu
 * @param pItem
 */
void menu_item_free(struct Menu* pMenu, struct MenuItem* pItem) {
	// Get the next and previous items
    struct MenuItem *Previous = pItem->pPrevious;
    struct MenuItem *Next = pItem->pNext;

    // Now we update the highlight index
    if (pMenu->Items.Count == 1)
        // if its 1, then the item we are removing is the item 1
        pMenu->HighlightedIndex = -1;
	else
        // if we are not one, that means we have more then 1 item, so we want to attempt
        // to find the nearest available item of the BUTTON type.
        pMenu->HighlightedIndex = menu_item_find_nearest_type_index(pMenu, pItem, IT_BUTTON, true);

    // Check if Previous is null (then we are the first element)
    //      and Next is set.
    //      - We are removing the first element
    if (Previous == NULL && Next != NULL) {
        // We now want to set Next to the first item in the array
        pMenu->Items.pFirst = Next;
        pMenu->Items.Count--;

        // Remove the pointer from Next->Previous
        Next->pPrevious = NULL;
    }

    // Check if Previous is not NULL but Next is
    //      - We are removing the last element
    else if (Previous != NULL && Next == NULL) {
        // Now we want to set the last element to Previous
        pMenu->Items.pLast = Previous;
        pMenu->Items.Count--;

        // Remove the pointer from Previous->Next
        Previous->pNext = NULL;
    }

    // If Previous and Next are NULL then we are removing
    //      the last item from the list
    //      - We are resetting the list and removing all elements
    else if (Previous == NULL && Next == NULL) {
        pMenu->Items.Count = 0;
        pMenu->Items.pLast = NULL;
        pMenu->Items.pFirst = NULL;
        pMenu->pSelectedItem = NULL;
    }

    // We are removing an item in the middle of the array
    //      - Updating the Next and Previous items.
    //
    //  This is basically the else condition but its written like this
    //  even though it is always TRUE when reached to make the code more
    //  readable.
    else if (Previous != NULL && Next != NULL) {
        // Update the Next and Previous pointers
        Previous->pNext = Next;
        Next->pPrevious = Previous;
        pMenu->Items.Count--;
    }

    // Remove the selected item if its pItem
    if (pMenu->pSelectedItem == pItem)
        pMenu->pSelectedItem = NULL;

    // AND FINALLY we free up the item
    free(pItem);
}

/**
 * Free the menu and its items
 *
 * @param pMenu
 */
void menu_item_free_all(struct Menu *pMenu) {
    // Get the first item
    struct MenuItem* pItem = pMenu->Items.pFirst;

    // Go through the linked list
    while (pItem != NULL) {
        struct MenuItem* currentItem = pItem;
        pItem = pItem->pNext;

        // Free our menu item
        free(currentItem);
    }

    pMenu->pSelectedItem = NULL;
    pMenu->Items.Count = 0;
}

void main_menu_prerender(WINDOW* win, struct Menu* menu) {
    wprintw(win, "Hello From prerender\n");
}

int main(void) {
    // Clear the window screen
    clrscr();

    // Setup the environment
    environment_setup();

    printw("Attach a debugger now!");
    refresh();
    getch();

    // Menu setup and settings
    struct Menu* menu = menu_create("Test Menu");
    menu->Settings.RenderSize = 10;
    menu->Settings.pRenderCb = &main_menu_prerender;

    // Menu contents
   	menu_item_add(menu, IT_TEXT,   "Simple Label - 1", -1);
    menu_item_add(menu, IT_BUTTON, "Simple Button 1", -1);
    menu_item_add(menu, IT_TEXT,   "Simple Label - 2", -1);
    menu_item_add(menu, IT_BUTTON, "Simple Button 1", -1);
    menu_item_add(menu, IT_TEXT,   "Simple Label - 1", -1);
    menu_item_add(menu, IT_TEXT,   "Simple Label - 3", -1);
    menu_item_add(menu, IT_TEXT,   "Simple Label - 4", -1);
    menu_item_add(menu, IT_TEXT,   "Simple Label - 5", -1);
	menu_item_add(menu, IT_BUTTON, "Simple Button 2", -1);
    menu_item_add(menu, IT_BUTTON, "Simple Button 3", -1);
	menu_item_add(menu, IT_BUTTON, "Menu menu item", -1);
    menu_item_add(menu, IT_BUTTON, "Simple Button 4", -1);
    menu_item_add(menu, IT_BUTTON, "Simple Button 5", -1);
    menu_item_add(menu, IT_BUTTON, "Simple Button 6", -1);
    menu_item_add(menu, IT_BUTTON, "Simple Button 7", -1);
    menu_item_add(menu, IT_BUTTON, "Simple Button 8", -1);
    menu_item_add(menu, IT_TEXT,   "Simple Label - 6", -1);

    // Render menu
    int menu_state = menu_render(stdscr, menu);

    // Selected item
    printw("Menu state %d, Highlight Index = %d\n", menu_state, menu->HighlightedIndex);

    if (menu->pSelectedItem != NULL)
         printw("Item Text: %s\n", menu->pSelectedItem->Text);
    else printw(">>>> NO ITEM SELECTED! <<<<\n");

    printw("After Window!\n"); refresh();

    printw("Freeing menu!\n"); refresh(); getch();
    menu_free(menu, true);
    printw("DONE!!!!\n"); refresh(); getch();

    // Clear the environment
    environment_end();
}

/**
 * Render the menu item
 *
 * @param pMenu
 */
void menu_render_item(WINDOW *pWin, int index, struct Menu *this, struct MenuItem *item) {
    // Text display
    if (item->Type == IT_TEXT) {
        // Italics or Bold
        wprintw(pWin, "  %s\t[%d](%p)\n", item->Text, index, item);
	}
    // Buttons
    else if (item->Type == IT_BUTTON) {
        // If we are the selection
        if (index == this->HighlightedIndex) {
            wprintw(pWin, " ");
            attron(A_REVERSE);
            wprintw(pWin, " > %s < ", item->Text);
            attroff(A_REVERSE);
       		wprintw(pWin, "\t[%d](%p)\n", index, item);
		}

        // If we are not then just render normally.
        else {
            wprintw(pWin, "    %s   \t[%d](%p)\n", item->Text, index, item);
		}
    }
}

/**
 * Move the selection down (and the viewport) in the menu
 *
 * @param pMenu
 */
void menu_render_move_down(struct Menu *pMenu) {
    // Loop until we have our item
    struct MenuItem *pItem = pMenu->Items.pFirst;
    bool foundButton = false;

    int i = 0;
    while (pItem != NULL) {
        // If we are the highlighted index
        if (i == pMenu->HighlightedIndex) {
            // Check if we have a next item increment our index
            if (pItem->pNext != NULL) {
                // We now need to loop to find the next button
                struct MenuItem *pButton = pItem->pNext;
                i++;

                while(pButton != NULL) {
                    // If we are a button then we are updating the highlight index
                    if (pButton->Type == IT_BUTTON) {
                        pMenu->HighlightedIndex = i;
                        foundButton = true;
                        goto viewport;
                    }

                    pButton = pButton->pNext;
                    i++;
                }
            }

            goto viewport;
        }

        pItem = pItem->pNext;
        i++;
    }

viewport: 
	// If the button is not rendered, move into view
	if (foundButton) {
		int hi = pMenu->HighlightedIndex;
		int vsi = pMenu->Settings.ViewStartIndex;
		int rs = pMenu->Settings.RenderSize;
		
		// If we are not in the view 
		if (hi > (vsi + rs - 1)) {
			pMenu->Settings.ViewStartIndex++;

			// If we still need to scroll then set Highlight index to the bottom of the view.
			if (hi > (vsi + rs)) {
				int newvsi = hi - vsi - 1;
				if (newvsi < 0) newvsi = 0;

				pMenu->Settings.ViewStartIndex = newvsi;	
			}
		}
	} else {
		// Just increment the view start index if we can
		if (pMenu->Settings.ViewStartIndex < (pMenu->Items.Count - pMenu->Settings.RenderSize)) {
			pMenu->Settings.ViewStartIndex++;
		}
	}
}

/**
 * Move the selection up (and the viewport) in the menu
 * @param pMenu
 */
void menu_render_move_up(struct Menu *pMenu) {
    // Loop until we have our item
    bool foundButton = false;

    // We cant go any higher then 0
    if (pMenu->HighlightedIndex == 0)
        goto viewport;

    struct MenuItem *pItem = pMenu->Items.pFirst;
    int i = 0;
    while (pItem != NULL) {
        // We found the current item
        if (i == pMenu->HighlightedIndex) {
            // Check if we have a next item increment our index
            if (pItem->pPrevious != NULL) {

                // We now need to loop to find the next button
                struct MenuItem *pButton = pItem->pPrevious; i--;
                while (pButton != NULL) {
                    // If we are a button then set the highlighted index
                    if (pButton->Type == IT_BUTTON) {
                        pMenu->HighlightedIndex = i;

                        // Set found button to true and update the viewport.
                        foundButton = true;
                        goto viewport;
                    }

                    // Loop next item
                    pButton = pButton->pPrevious;
                    i--;
                }
            }

            // If we could not find anything update the viewport.
            goto viewport;
        }

        // Loop next item.
        pItem = pItem->pNext;
        i++;
    }

viewport:
    if (foundButton) {
        // Move the button into view if its not current visible
        if (pMenu->HighlightedIndex < pMenu->Settings.ViewStartIndex)
            pMenu->Settings.ViewStartIndex = pMenu->HighlightedIndex;
    } else {
        // If we can still scroll
        if (pMenu->Settings.ViewStartIndex > 0)
            // Scroll up
            pMenu->Settings.ViewStartIndex--;
    }
}

/**
 * Finds and sets the current active Item
 *
 * @param pMenu
 * @return
 */
int menu_render_select_highlighted(struct Menu *pMenu) {
    // Loop until we have our item
    struct MenuItem* pItem = pMenu->Items.pFirst;
    int i = 0;
    while (pItem != NULL) {
        // We found our item
        if (i == pMenu->HighlightedIndex) {
            // Check if index is not a button
            if (pItem->Type != IT_BUTTON)
                return 1; // We found it

            // Set the selected item
            pMenu->pSelectedItem = pItem;
            return 0; // Failed to find it
        }

        // Get our next item
        pItem = pItem->pNext;
        i++;
    }

    // Keep rendering
    return 1;
}

#ifdef DEBUG

/**
 * DEBUG: Add a item to the end of the list.
 *
 * @param pWin
 * @param pMenu
 * @param type
 */
void menu_dbg_add_item(WINDOW* pWin, struct Menu *pMenu, enum ItemType type) {
    wclear(pWin);
    wprintw(pWin, "Enter a button name: ");
	
    echo();
    char input[MENU_TEXT_SIZE];
    wgetstr(pWin, input);
    noecho();
	
    menu_item_add(pMenu, type, input, -1);
    wclear(pWin);
}

/**
 * DEBUG: Delete the highlighted item
 *
 * @param pMenu
 */
void menu_dbg_delete_highlighted(struct Menu *pMenu) {
    struct MenuItem *pItem = pMenu->Items.pFirst;
    int i = 0;
    while (pItem  != NULL) {
        if (i == pMenu->HighlightedIndex) {
            menu_item_free(pMenu, pItem); // This should be freed
            return;
        }

        pItem = pItem->pNext;
        i++;
    }
}

#endif

/**
 * Render the menu
 * @param pWin The window to render the menu onto
 * @param pMenu The menu to render
 * @return The menu exit state, 0=No selection, 1=Selection.
 */
int menu_render(WINDOW *pWin, struct Menu *pMenu) {
    // We are rendering
    int rendering = 1,
        result = 0; // Our return result
	
    // Reset our selection
    pMenu->pSelectedItem = NULL;

    // If we have items then we want to set the selection index
    if (pMenu->Items.Count > 0) {
        // If the first item is a button
        if (pMenu->Items.pFirst->Type == IT_BUTTON)
            pMenu->HighlightedIndex = 0; // Set the highlight index to the first item
        else // If the first item is not a button then we want to find the nearest button (going up)
            pMenu->HighlightedIndex = menu_item_find_next_type_index_up(pMenu, pMenu->Items.pFirst, IT_BUTTON, 0);
    }

    // Render our menu
    while(rendering == 1) {
        clear();

#if DEBUG
        // Print our header
        wprintw(pWin, "HI(%d) VSI(%d)\n", pMenu->HighlightedIndex, pMenu->Settings.ViewStartIndex);
		wprintw(pWin, "Item count: %d\n\n", pMenu->Items.Count);
        wprintw(pWin, "%s\n======-======-======-======\n", pMenu->Title);
#endif
        if (pMenu->Settings.pRenderCb != NULL)
            pMenu->Settings.pRenderCb(pWin, pMenu);

        // Just loop the list for now and render it
        struct MenuItem* pItem = pMenu->Items.pFirst;
        int i = 0, v = 1;
        while (pItem != NULL) {
            if (i >= pMenu->Settings.ViewStartIndex && v <= pMenu->Settings.RenderSize) {
                // Render the item
                menu_render_item(pWin, i, pMenu, pItem);
                v++;
            }

            // Next item
            pItem = pItem->pNext;
            i++;
        }

        // Get our key press
        int keyPress = wgetch(pWin);
        switch(keyPress) {
            case 10:                    // ENTER
                rendering = menu_render_select_highlighted(pMenu);
                result = 1;
                break;
            case 113:                   // Q
            case 27:                    // ESCAPE
                rendering = 0;
                result = 0;
                break;
            case 100:                   // D
                menu_dbg_delete_highlighted(pMenu);
                break;
            case 258:                   // DOWN_ARROW
                menu_render_move_down(pMenu);
				break;
            case 259:                   // UP_ARROW
                menu_render_move_up(pMenu);
				break;
#ifdef DEBUG
            case 260:                   // LEFT_ARROW
                break;
            case 261:                   // RIGHT_ARROW
                break;
            case 98:                    // Debug - B (add text)
                menu_dbg_add_item(pWin, pMenu, IT_TEXT);
                break;
            case 97:                    // Debug - A (add button)
                menu_dbg_add_item(pWin, pMenu, IT_BUTTON);
                break;
#endif
            default:
                break;
        }

        // Refresh the screen
        wrefresh(pWin);
    }

    return result;
}
